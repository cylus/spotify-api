package pl.cylkowski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.cylkowski.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findUserByUsername(String username);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE users SET activated = true WHERE id = :id", nativeQuery = true)
    void activateUser(@Param("id") long id);

}