package pl.cylkowski;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.cylkowski.model.Role;
import pl.cylkowski.model.User;
import pl.cylkowski.repository.UserRepository;

@SpringBootApplication
public class ApiProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiProjectApplication.class, args);
    }

//    @Bean
//    CommandLineRunner commandLineRunner(UserRepository repository, BCryptPasswordEncoder encoder) {
//        return args -> {
//            repository.save(new User("user", encoder.encode("user"), Role.ROLE_USER));
//            repository.save(new User("admin", encoder.encode("admin"), Role.ROLE_ADMIN));
//        };
//    }

}
