package pl.cylkowski.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record UserRequest(@NotBlank(message = "Pole 'username' nie może być puste") String username,
                          @NotBlank(message = "Pole 'hasło' nie może być puste") String password,
                          @NotBlank(message = "Pole 'potwiedź hasło' nie może być puste") String confirmPassword,
                          @Email String email) {
}