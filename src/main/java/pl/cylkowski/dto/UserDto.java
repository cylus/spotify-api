package pl.cylkowski.dto;

import pl.cylkowski.model.Role;

public record UserDto(long id, String username, String email, Role role) {

}