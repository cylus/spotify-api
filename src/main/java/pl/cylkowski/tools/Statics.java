package pl.cylkowski.tools;

public class Statics {

    public static final String TEMPLATE_HTML = """
            Kliknij link poniżej i aktywowuj swoje konto:
                        
            %s
                        
            Pozdrawiamy,
                        
            Team Twojej aplikacji
            """;

    public static final String REGISTRATION_SUBJECT = "Aktywuj swoje konto!";
}
