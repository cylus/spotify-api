package pl.cylkowski.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.cylkowski.dto.UserDto;
import pl.cylkowski.dto.UserRequest;
import pl.cylkowski.mail.MailService;
import pl.cylkowski.model.User;
import pl.cylkowski.repository.UserRepository;

import java.util.Date;

import static pl.cylkowski.tools.Statics.REGISTRATION_SUBJECT;
import static pl.cylkowski.tools.Statics.TEMPLATE_HTML;

@Service
@RequiredArgsConstructor
public class UserService {

    @Value("${base.activation.url}")
    private String baseActivateUrl;

    private final MailService mailService;

    private final PasswordEncoder encoder;

    private final UserRepository userRepository;

    private final TokenGenerateService tokenGenerateService;

    public UserDto createUser(UserRequest request) {
        User savedUser = userRepository.save(new User(request, encoder.encode(request.password())));
        sendActivationMail(savedUser.getEmail(), String.valueOf(savedUser.getId()));
        return new UserDto(savedUser.getId(), savedUser.getUsername(), savedUser.getEmail(), savedUser.getRole());
    }

    protected void sendActivationMail(String email, String id) {
        mailService.sendEmail(email, REGISTRATION_SUBJECT, createContent(baseActivateUrl, id));
    }

    private String createContent(String baseActivateUrl, String id) {
        return TEMPLATE_HTML.formatted(baseActivateUrl + tokenGenerateService.generateToken(id));
    }

    public String activate(String token) {
        DecodedJWT decode = JWT.decode(token);
        isTokenExpired(decode.getExpiresAt());
        String issuerId = decode.getIssuer();
        userRepository.activateUser(Long.parseLong(decode.getIssuer()));
        return "User is activated";
    }

    protected boolean isTokenExpired(Date expiresAt) {
        if (new Date().after(expiresAt))
            throw new IllegalArgumentException("Token wygasł");
        return false;
    }
}