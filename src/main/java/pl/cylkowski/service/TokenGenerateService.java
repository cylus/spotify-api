package pl.cylkowski.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TokenGenerateService {

    @Value("${jwt.secrete}")
    private String jwtSecrete;

    public String generateToken(String id) {
        return JWT.create()
                .withIssuer(id)
                .withExpiresAt(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24))
                .sign(Algorithm.HMAC256(jwtSecrete.getBytes()));
    }
}