package pl.cylkowski.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.cylkowski.dto.UserRequest;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique = true)
    private String username;
    private String password;
    @Column(unique = true)
    private String email;
    @Enumerated(EnumType.STRING)
    private Role role;
    private boolean activated;


    public User(UserRequest request, String encodePassword) {
        this.username = request.username();
        this.password = encodePassword;
        this.email = request.email();
        this.role = Role.ROLE_USER;
        this.activated = false;
    }
}