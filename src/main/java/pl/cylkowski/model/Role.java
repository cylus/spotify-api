package pl.cylkowski.model;

public enum Role {

    ROLE_USER, ROLE_ADMIN
}