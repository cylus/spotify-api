package pl.cylkowski.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.cylkowski.dto.UserDto;
import pl.cylkowski.dto.UserRequest;
import pl.cylkowski.model.Role;
import pl.cylkowski.model.User;
import pl.cylkowski.repository.UserRepository;
import pl.cylkowski.service.TokenGenerateService;
import pl.cylkowski.service.UserService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenGenerateService tokenGenerateService;

    private final ObjectMapper om = new ObjectMapper();

    @Test
    void createUserTest() throws Exception {
        mockMvc.perform(post("http://localhost:8080/user/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(new UserRequest("test", "pass", "pass", "test@test.pl"))))
                .andExpect(status().isCreated())
                .andExpect(content().json(om.writeValueAsString(new UserDto(1, "test", "test@test.pl", Role.ROLE_USER))));
    }

    @Test
    void activateUserTest() throws Exception {
        String token = tokenGenerateService.generateToken("1");
        userRepository.save(new User(1, "test", "test", "test@test.pl", Role.ROLE_USER, false));

        mockMvc.perform(get("http://localhost:8080/user/activate/" + token)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        boolean isActivated = userRepository.findById(1L).get().isActivated();
        Assertions.assertThat(isActivated).isTrue();
    }
}