package pl.cylkowski.repository;

import jakarta.transaction.Transactional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.cylkowski.model.Role;
import pl.cylkowski.model.User;

import java.util.List;

@Transactional
@SpringBootTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void activateUserTest() {
        userRepository.save(new User(1L, "test", "test", "test@test.pl", Role.ROLE_USER, false));
        userRepository.save(new User(2L, "test1", "test", "test@test1.pl", Role.ROLE_USER, false));
        userRepository.save(new User(3L, "test2", "test", "test@test2.pl", Role.ROLE_USER, false));
        userRepository.save(new User(4L, "test3", "test", "test@test3.pl", Role.ROLE_USER, false));
        userRepository.activateUser(2L);
        boolean isActivated = userRepository.findById(2L).get().isActivated();
        Assertions.assertThat(isActivated).isTrue();
    }
}