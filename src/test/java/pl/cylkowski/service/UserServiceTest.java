package pl.cylkowski.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private TokenGenerateService tokenGenerateService;

    @Test
    void sendActivationMailTest() {
        userService.sendActivationMail("marcin.cylkowski3@gmail.com", "1");
    }

    @Test
    void isAfterTest() {
        DecodedJWT decode = JWT.decode("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxIiwiZXhwIjoxNjg5MDkyODI1fQ.bbTHu2Yk8TLpeWHLrAcJHC3G6rx8B9pZRgjJsQEKEJA");
        boolean after = userService.isTokenExpired(decode.getExpiresAt());
        Assertions.assertThat(after).isFalse();
    }

    @Test
    void generateTokenTest() {
        String token = tokenGenerateService.generateToken("1");
        Assertions.assertThat(token).isNotNull();
    }
}